import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import '../models/user_model.dart';
import '../router/router.gr.dart';
import '../services/api_service.dart';

class MyHomePage extends StatefulWidget {
  final textValue;
  const MyHomePage({
required this.textValue,
  });

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  ApiService service = ApiService();
  List data = [];
  List<UserModel> users = [];
 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Title"),
        ),
        body: Container(
          margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 5),
        padding: const EdgeInsets.symmetric(vertical: 10.0,horizontal:7.0),
          child: Column(
            children: [
              ElevatedButton(
                child: const Text("goto second"),
                onPressed: () {
                  context.router.push( SecondRoute(msg: widget.textValue));
                },
              ),
             Text(widget.textValue),

              futureM(),
            ],
          ),
        ));
  }

  FutureBuilder<dynamic> futureM() {
    return FutureBuilder(
      future: service.fetchData(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(child: CircularProgressIndicator());
        } else if (snapshot.hasData) {
          data = snapshot.data;
          users = data.map((e) => UserModel.fromJson(e)).toList();
          print(users);
          return SizedBox(
            height: 500,
            width: double.infinity,
            child: ListView.builder(
              itemBuilder: (context, index) {
                return ListTile(
                  title: Text(users[index].username),
                  subtitle: Text(users[index].id.toString()),
                );
              },
              itemCount: users.length,
            ),
          );
        } else {
          return const Text("Error");
        }
      },
    );
  }
}