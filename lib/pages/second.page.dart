import 'package:flutter/material.dart';

class SecondPage extends StatelessWidget {
  final  msg;
  const SecondPage({super.key, this.msg});

  @override
  Widget build(BuildContext context) {
    return  Scaffold(appBar:AppBar(title: const Text('Second Page'),) ,body: Center(child: Text("Second $msg")),);
  }
}