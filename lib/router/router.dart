              
// @CupertinoAutoRouter              
// @AdaptiveAutoRouter              
// @CustomAutoRouter              
import 'package:auto_route/annotations.dart';

import '../pages/home.page.dart';
import '../pages/login.page.dart';
import '../pages/second.page.dart';

@MaterialAutoRouter(              
  replaceInRouteName: 'Page,Route',              
  routes: <AutoRoute>[              
    AutoRoute(page: LoginPage, initial: true,name: "Login"),   
     AutoRoute(page: MyHomePage,),   
     AutoRoute(page: SecondPage,), 
           
            
  ],              
)              
class $AppRouter {} 