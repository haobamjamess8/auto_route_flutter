import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';
class ApiService{
  
  final String URL = "https://jsonplaceholder.typicode.com/users";

  Future<dynamic> fetchData() async{
      Response response = await http.get(Uri.parse(URL));
      List datas = [];
      if(response.statusCode == 200){
       datas =   jsonDecode(response.body);
      }
      else{
        print(response.statusCode);
      }
return datas;
  }

}