// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i4;
import 'package:flutter/material.dart' as _i5;

import '../pages/home.page.dart' as _i2;
import '../pages/login.page.dart' as _i1;
import '../pages/second.page.dart' as _i3;

class AppRouter extends _i4.RootStackRouter {
  AppRouter([_i5.GlobalKey<_i5.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i4.PageFactory> pagesMap = {
    Login.name: (routeData) {
      final args = routeData.argsAs<LoginArgs>(orElse: () => const LoginArgs());
      return _i4.MaterialPageX<dynamic>(
        routeData: routeData,
        child: _i1.LoginPage(key: args.key),
      );
    },
    MyHomeRoute.name: (routeData) {
      final args = routeData.argsAs<MyHomeRouteArgs>();
      return _i4.MaterialPageX<dynamic>(
        routeData: routeData,
        child: _i2.MyHomePage(textValue: args.textValue),
      );
    },
    SecondRoute.name: (routeData) {
      final args = routeData.argsAs<SecondRouteArgs>(
          orElse: () => const SecondRouteArgs());
      return _i4.MaterialPageX<dynamic>(
        routeData: routeData,
        child: _i3.SecondPage(
          key: args.key,
          msg: args.msg,
        ),
      );
    },
  };

  @override
  List<_i4.RouteConfig> get routes => [
        _i4.RouteConfig(
          Login.name,
          path: '/',
        ),
        _i4.RouteConfig(
          MyHomeRoute.name,
          path: '/my-home-page',
        ),
        _i4.RouteConfig(
          SecondRoute.name,
          path: '/second-page',
        ),
      ];
}

/// generated route for
/// [_i1.LoginPage]
class Login extends _i4.PageRouteInfo<LoginArgs> {
  Login({_i5.Key? key})
      : super(
          Login.name,
          path: '/',
          args: LoginArgs(key: key),
        );

  static const String name = 'Login';
}

class LoginArgs {
  const LoginArgs({this.key});

  final _i5.Key? key;

  @override
  String toString() {
    return 'LoginArgs{key: $key}';
  }
}

/// generated route for
/// [_i2.MyHomePage]
class MyHomeRoute extends _i4.PageRouteInfo<MyHomeRouteArgs> {
  MyHomeRoute({required dynamic textValue})
      : super(
          MyHomeRoute.name,
          path: '/my-home-page',
          args: MyHomeRouteArgs(textValue: textValue),
        );

  static const String name = 'MyHomeRoute';
}

class MyHomeRouteArgs {
  const MyHomeRouteArgs({required this.textValue});

  final dynamic textValue;

  @override
  String toString() {
    return 'MyHomeRouteArgs{textValue: $textValue}';
  }
}

/// generated route for
/// [_i3.SecondPage]
class SecondRoute extends _i4.PageRouteInfo<SecondRouteArgs> {
  SecondRoute({
    _i5.Key? key,
    dynamic msg,
  }) : super(
          SecondRoute.name,
          path: '/second-page',
          args: SecondRouteArgs(
            key: key,
            msg: msg,
          ),
        );

  static const String name = 'SecondRoute';
}

class SecondRouteArgs {
  const SecondRouteArgs({
    this.key,
    this.msg,
  });

  final _i5.Key? key;

  final dynamic msg;

  @override
  String toString() {
    return 'SecondRouteArgs{key: $key, msg: $msg}';
  }
}
