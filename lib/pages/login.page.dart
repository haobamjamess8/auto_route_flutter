import 'package:api/router/router.gr.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  LoginPage({super.key});
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 5),
        padding: const EdgeInsets.symmetric(vertical: 10.0,horizontal:7.0),
        child: Column(
          children: [
            ElevatedButton(
              child: const Text("click"),
              onPressed: () {
                final textValue = controller.text;
                context.router.replace(MyHomeRoute(textValue: textValue));
              },
            ),
            SizedBox(
              child: TextField(
                controller: controller,
                decoration: const InputDecoration(hintText: "Enter any text"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
